package com.example.uuu9.canvastask;


import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import static android.R.attr.angle;
import static android.R.attr.x;
import static android.R.attr.y;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(this));
    }

    class DrawView extends View {
        Paint p;
        RectF rectf;
        float[] points;
        float[] points1;
        int x,y,x1,y1;

        public DrawView(Context context) {
            super(context);
            p = new Paint();
            points1 = new float[]{300,200,600,200,300,300,600,300,400,100,400,400,500,100,500,400};
            x = 0;
            y = 0;
            x1 = 0;
            y1 = 0;

        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawARGB(80, 102, 204, 255);

            p.setColor(Color.BLACK);
            p.setStrokeWidth(10);


            for(int i = 1; i<8;i++){
                for(int j=0;j<20;j++){
                    canvas.drawLine(x,y,x,y+100,p);
                    x = x+ 50;
                }
                x=0;
                y = y+ 200;
            }
            for(int i = 1; i<13;i++){
                for(int j=0;j<19;j++){
                    canvas.drawLine(x1,y1,x1+50,y1+100,p);
                    x1 = x1 + 50;
                }
                x1=0;
                y1 = y1+100;
            }


        }
    }

}